package me.mathiaseklund.survivalrpg.items;

import java.util.ArrayList;
import java.util.List;

import org.bukkit.Bukkit;
import org.bukkit.ChatColor;
import org.bukkit.Material;
import org.bukkit.configuration.ConfigurationSection;
import org.bukkit.enchantments.Enchantment;
import org.bukkit.inventory.ItemFlag;
import org.bukkit.inventory.ItemStack;
import org.bukkit.inventory.meta.ItemMeta;

public class ItemBuilder {

	ItemStack itemstack;

	Material material;

	int amount = 1;

	boolean glow = false;

	String displayname;
	String identifier;

	List<String> lore;

	//
	// CONSTRUCTORS
	//

	/**
	 * Constructs a new ItemBuilder, without any content.
	 */
	public ItemBuilder() {

	}

	/**
	 * Constructs a new ItemBuilder
	 * 
	 * @param is
	 *            ItemStack to build
	 */
	public ItemBuilder(ItemStack itemstack) {
		this.itemstack = itemstack;
		getIdentifier(itemstack);
		getDisplayName(itemstack);
		getLore(itemstack);
		getMaterial(itemstack);
		getAmount(itemstack);
	}

	/**
	 * Constructs a new ItemBuilder
	 * 
	 * @param material
	 *            Type of Item to build
	 */
	public ItemBuilder(Material material) {
		this.material = material;
	}

	/**
	 * Constructs a new ItemBuilder
	 * 
	 * @param material
	 *            Type of Item to build
	 * @param amount
	 *            Item amount
	 */
	public ItemBuilder(Material material, int amount) {
		this.material = material;
		this.amount = amount;
	}

	//
	// SERIALIZATION
	//

	/**
	 * Loads the item from a {@link ConfigurationSection}
	 * 
	 * @param section
	 *            Section of the configuration containing the item
	 * @return the ItemBuilder object
	 */
	public ItemBuilder fromConfig(ConfigurationSection section) {
		if (section == null) {
			throw new IllegalArgumentException("ConfigurationSection cannot be NULL");
		}
		if (section.contains("material")) {
			if (section.isString("material")) {
				Material material = null;
				try {
					material = Material.getMaterial(section.getString("material").toUpperCase());
				} catch (Exception e) {
					// error
				}
				if (material != null) {
					setMaterial(material);
				} else {
					Bukkit.getLogger().warning("Error getting Material from Config");
				}
			} else {
				throw new IllegalArgumentException("Material has to be a String. Example: DIAMOND");
			}
		}
		if (section.contains("identifier")) {
			if (section.isString("identifier")) {
				String identifier = section.getString("identifier");
				setIdentifier(identifier);
			} else {
				throw new IllegalArgumentException("Identifier has to be a String. Example: custom_item");
			}
		}
		if (section.contains("amount")) {
			if (section.isInt("amount")) {
				int amount = section.getInt("amount");
				setAmount(amount);
			} else {
				throw new IllegalArgumentException("Amount has to be an Integer. Example: 1");
			}
		}
		if (section.contains("displayname")) {
			if (section.isString("displayname")) {
				String displayname = section.getString("displayname");
				setDisplayName(displayname);
			}
		}
		if (section.contains("lore")) {
			if (section.isList("lore")) {
				List<String> lore = section.getStringList("lore");
				setLore(lore);
			} else {
				throw new IllegalArgumentException(
						"Lore has to be a List. Example: [] OR - 'lore text' on a new line under lore:");
			}
		}
		if (section.contains("glow")) {
			if (section.isBoolean("glow")) {
				boolean glow = section.getBoolean("glow");
				setGlow(glow);
			} else {
				throw new IllegalArgumentException("Glow has to be a Boolean. Example: true || false");
			}
		}

		return this;
	}

	/**
	 * Writes the Item to a {@link ConfigurationSection}
	 * 
	 * @param section
	 *            {@link ConfigurationSection}
	 * @return return the resulting {@link ConfigurationSection}
	 */
	public ConfigurationSection toConfig(ConfigurationSection section) {
		if (section == null) {
			throw new IllegalArgumentException("ConfigurationSection cannot be NULL");
		}
		section.set("material", getMaterial().toString());
		section.set("amount", getAmount());
		section.set("displayname", getDisplayName());
		section.set("lore", getLore());
		section.set("identifier", getIdentifier());
		section.set("glow", getGlow());
		return section;
	}

	//
	// SETTERS
	//

	/**
	 * Set the ItemStack Material type
	 * 
	 * @param material
	 *            Material to use
	 */
	public void setMaterial(Material material) {
		this.material = material;
	}

	/**
	 * Set the ItemStack stack size
	 * 
	 * @param amount
	 *            Amount of items in the stack.
	 */
	public void setAmount(int amount) {
		if (amount < 1) {
			amount = 1;
		}
		this.amount = amount;
	}

	/**
	 * Set the ItemStack displayname
	 * 
	 * @param displayname
	 *            The displayname String
	 */
	public void setDisplayName(String displayname) {
		this.displayname = displayname;
	}

	/**
	 * Set the ItemStack lore content
	 * 
	 * @param lore
	 *            The lore List<String>
	 */
	public void setLore(List<String> lore) {
		this.lore = lore;
	}

	/**
	 * Set the ItemStack Identifier This can be used to identify the item as what it
	 * is
	 * 
	 * @param identifier
	 *            The identifier string
	 */
	public void setIdentifier(String identifier) {
		identifier = identifier.replaceAll(" ", "_");
		this.identifier = identifier;
	}

	/**
	 * Set if the itemstack should have a glow effect on it.
	 * 
	 * @param glow
	 *            true/false
	 */
	public void setGlow(boolean glow) {
		this.glow = glow;
	}
	//
	// GETTERS
	//

	/**
	 * Get the ItemStack Material type
	 * 
	 * @return The material type
	 */
	public Material getMaterial() {
		return this.material;
	}

	public Material getMaterial(ItemStack is) {
		Material mat = is.getType();
		setMaterial(mat);
		return mat;
	}

	/**
	 * Get the ItemStack stack size
	 * 
	 * @return amount of items in the stack
	 */
	public int getAmount() {
		return this.amount;
	}

	public int getAmount(ItemStack is) {
		int amount = is.getAmount();
		setAmount(amount);
		return amount;
	}

	/**
	 * Get the DisplayName of the ItemStack
	 * 
	 * @return The displayname String
	 */
	public String getDisplayName() {
		return this.displayname;
	}

	public String getDisplayName(ItemStack is) {
		String displayname = null;

		if (is.hasItemMeta()) {
			ItemMeta im = is.getItemMeta();
			if (im.hasDisplayName()) {
				displayname = im.getDisplayName();
			}
		}
		setDisplayName(displayname);

		return displayname;
	}

	/**
	 * Get the Identifier of the ItemStack
	 * 
	 * @return The identifier String
	 */
	public String getIdentifier() {
		return this.identifier;
	}

	public String getIdentifier(ItemStack is) {
		String identifier = null;
		if (is.hasItemMeta()) {
			ItemMeta im = is.getItemMeta();
			if (im.hasDisplayName()) {
				String name = im.getDisplayName();
				name = unhide(name);
				if (name.contains("id")) {
					identifier = name.split("\\]")[0];
					identifier = identifier.split("\\[id")[1];
					return identifier;
				}
			}
		}
		return identifier;
	}

	/**
	 * Get the ItemStack lore content
	 * 
	 * @return the lore List<String>
	 */
	public List<String> getLore() {
		return this.lore;
	}

	public List<String> getLore(ItemStack is) {
		List<String> lore = new ArrayList<String>();
		if (is.hasItemMeta()) {
			ItemMeta im = is.getItemMeta();
			if (im.hasLore()) {
				lore = im.getLore();
			}
		}
		return lore;
	}

	/**
	 * Get the ItemStack associated with this ItemBuilder
	 * 
	 * @return the ItemStack if its built. NULL if not built.
	 */
	public ItemStack getItemStack() {
		return this.itemstack;
	}

	/**
	 * Get the Glow status of the ItemStack
	 * 
	 * @return true/false
	 */
	public boolean getGlow() {
		return this.glow;
	}

	//
	// BUILDING
	//

	/**
	 * Check if the ItemStack has been built.
	 * 
	 * @return true or false boolean
	 */
	public boolean isBuilt() {
		return (getItemStack() != null);
	}

	/**
	 * Build the ItemStack from the set Data
	 * 
	 * @return New, freshly built ItemStack
	 */
	public ItemStack build() {
		Material mat = getMaterial();
		int amount = getAmount();
		String displayname = getDisplayName();
		List<String> lore = new ArrayList<String>();
		for (String s : getLore()) {
			lore.add(ChatColor.translateAlternateColorCodes('&', s));
		}

		ItemStack is = null;
		ItemMeta im = null;

		if (material != null) {
			is = new ItemStack(mat);
		}
		if (is != null) {
			is.setAmount(amount);
			im = is.getItemMeta();
			im.setLore(lore);
			if (displayname != null) {
				im.setDisplayName(
						hide("[id" + identifier + "]") + ChatColor.translateAlternateColorCodes('&', displayname));
			}

			im.addItemFlags(ItemFlag.HIDE_ENCHANTS);

			is.setItemMeta(im);

			if (getGlow()) {
				is.addUnsafeEnchantment(Enchantment.PROTECTION_FALL, 1);
			}

		}

		return is;
	}

	/**
	 * Turn a string invisible.
	 * 
	 * @param string
	 *            String to make Invisible
	 * @return Invisible String
	 */
	public static String hide(String string) {
		StringBuilder builder = new StringBuilder();

		for (char c : string.toCharArray()) {
			builder.append(ChatColor.COLOR_CHAR).append(c);
		}
		return builder.toString();
	}

	/**
	 * Turn a hidden string visible.
	 * 
	 * @param hidden
	 *            String that is Invisible
	 * @return Visible String
	 */
	public static String unhide(String hidden) {
		if (hidden != null) {
			return hidden.replaceAll(String.valueOf(ChatColor.COLOR_CHAR), "");
		} else {
			return null;
		}
	}
}
