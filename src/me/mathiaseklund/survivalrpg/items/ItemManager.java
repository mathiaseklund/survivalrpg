package me.mathiaseklund.survivalrpg.items;

import java.io.File;
import java.util.HashMap;

import org.bukkit.Material;
import org.bukkit.configuration.ConfigurationSection;
import org.bukkit.configuration.file.FileConfiguration;
import org.bukkit.configuration.file.YamlConfiguration;
import org.bukkit.entity.Player;
import org.bukkit.inventory.ItemStack;

import me.mathiaseklund.survivalrpg.Main;
import me.mathiaseklund.survivalrpg.util.Util;

public class ItemManager {

	Main main;

	File f;
	FileConfiguration data;

	HashMap<String, ItemStack> customitems = new HashMap<String, ItemStack>();

	public ItemManager() {
		main = Main.getMain();

		loadCustomItems();
	}

	// PRIVATE VOIDS

	private void loadCustomItems() {
		this.f = new File(main.getDataFolder() + "/customitems.yml");
		this.data = YamlConfiguration.loadConfiguration(this.f);
	}

	// PUBLIC VOIDS

	public void giveCustomItem(Player player, String itemId, int amount) {
		int firstEmpty = player.getInventory().firstEmpty();
		Util.debug("FirstEmpty: " + firstEmpty);
		player.getInventory().setItem(firstEmpty, getCustomItem(itemId, amount));
	}

	// DOUBLES

	public double getMinecraftAttackDamage(ItemStack item) {
		double damage = 0;
		Material mat = item.getType();

		// SWORDS

		if (mat == Material.DIAMOND_SWORD) {
			damage = 7;
		} else if (mat == Material.GOLDEN_SWORD) {
			damage = 4;
		}
		if (mat == Material.STONE_SWORD) {
			damage = 5;
		}
		if (mat == Material.WOODEN_SWORD) {
			damage = 4;
		}
		if (mat == Material.IRON_SWORD) {
			damage = 6;
		}

		// AXES

		if (mat == Material.DIAMOND_AXE) {
			damage = 9;
		}
		if (mat == Material.GOLDEN_AXE) {
			damage = 7;
		}
		if (mat == Material.STONE_AXE) {
			damage = 9;
		}
		if (mat == Material.IRON_AXE) {
			damage = 9;
		}
		if (mat == Material.WOODEN_AXE) {
			damage = 7;
		}

		// SHOVELS

		if (mat == Material.DIAMOND_SHOVEL) {
			damage = 5.5;
		}
		if (mat == Material.GOLDEN_SHOVEL) {
			damage = 2.5;
		}
		if (mat == Material.STONE_SHOVEL) {
			damage = 3.5;
		}
		if (mat == Material.IRON_SHOVEL) {
			damage = 4.5;
		}
		if (mat == Material.WOODEN_SHOVEL) {
			damage = 2.5;
		}

		// PICKAXES

		if (mat == Material.DIAMOND_PICKAXE) {
			damage = 5;
		}
		if (mat == Material.GOLDEN_PICKAXE) {
			damage = 2;
		}
		if (mat == Material.STONE_PICKAXE) {
			damage = 3;
		}
		if (mat == Material.IRON_PICKAXE) {
			damage = 4;
		}
		if (mat == Material.WOODEN_PICKAXE) {
			damage = 2;
		}
		return damage;
	}

	public double getDamage(ItemStack item) {
		return getMinecraftAttackDamage(item);
	}

	// ITEMSTACKS

	public ItemStack getCustomItem(String itemId, int amount) {
		ItemStack is = null;
		if (customitems.containsKey(itemId)) {
			is = new ItemStack(customitems.get(itemId));
			is.setAmount(amount);
		} else {
			ItemBuilder builder = new ItemBuilder();
			ConfigurationSection section = data.getConfigurationSection(itemId);
			builder.fromConfig(section);
			is = builder.build();
			is.setAmount(amount);
		}
		return is;
	}

}
