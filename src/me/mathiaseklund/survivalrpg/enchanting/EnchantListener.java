package me.mathiaseklund.survivalrpg.enchanting;

import org.bukkit.Bukkit;
import org.bukkit.Material;
import org.bukkit.block.Block;
import org.bukkit.enchantments.Enchantment;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.block.Action;
import org.bukkit.event.enchantment.EnchantItemEvent;
import org.bukkit.event.enchantment.PrepareItemEnchantEvent;
import org.bukkit.event.inventory.ClickType;
import org.bukkit.event.inventory.InventoryClickEvent;
import org.bukkit.event.inventory.InventoryCloseEvent;
import org.bukkit.event.inventory.InventoryDragEvent;
import org.bukkit.event.inventory.InventoryOpenEvent;
import org.bukkit.event.inventory.InventoryType;
import org.bukkit.event.player.PlayerInteractEvent;
import org.bukkit.inventory.AnvilInventory;
import org.bukkit.inventory.Inventory;
import org.bukkit.inventory.InventoryView;
import org.bukkit.inventory.InventoryView.Property;
import org.bukkit.inventory.ItemStack;
import org.bukkit.inventory.meta.EnchantmentStorageMeta;

import me.mathiaseklund.survivalrpg.Main;
import me.mathiaseklund.survivalrpg.items.ItemBuilder;
import me.mathiaseklund.survivalrpg.util.Util;

public class EnchantListener implements Listener {

	Main main = Main.getMain();

	EnchantManager manager = Main.getEnchantManager();

	@EventHandler
	public void onPrepareItemEnchant(PrepareItemEnchantEvent event) {
		InventoryView view = event.getView();
		int bonus = event.getEnchantmentBonus();
		int button1 = (int) Math.floor((bonus - 14) / 7);
		int button2 = (int) Math.floor((bonus - 7) / 7);
		int button3 = (int) Math.floor(bonus / 7);

		if (button1 < 1) {
			button1 = 0;
		}
		if (button2 < 1) {
			button2 = 0;
		}
		if (button3 < 1) {
			button3 = 0;
		}

		if (view != null) {
			view.setProperty(Property.ENCHANT_BUTTON1, button1);
			view.setProperty(Property.ENCHANT_BUTTON2, button2);
			view.setProperty(Property.ENCHANT_BUTTON3, button3);
			view.setProperty(Property.ENCHANT_ID1, -1);
			view.setProperty(Property.ENCHANT_ID2, -1);
			view.setProperty(Property.ENCHANT_ID3, -1);
		}

		// TODO add enchant currency item.
	}

	@EventHandler
	public void onOpenInventory(InventoryOpenEvent event) {
		if (event.getInventory().getType() == InventoryType.ENCHANTING) {
			event.getInventory().setItem(1, new ItemStack(Material.LAPIS_LAZULI));
		}
	}

	@EventHandler
	public void onEnchantItem(EnchantItemEvent event) {
		Player player = event.getEnchanter();
		event.setExpLevelCost(0);
		event.getEnchantsToAdd().clear();

		Util.debug(player.getName() + " wants to enchant an item.");
	}

	@SuppressWarnings("deprecation")
	@EventHandler
	public void onInventoryClick(InventoryClickEvent event) {
		InventoryView view = event.getView();
		int rawslot = event.getRawSlot();
		Util.debug("Raw Slot: " + rawslot);
		if (view.getTopInventory().getType() == InventoryType.ENCHANTING) {
			if (event.getClick() == ClickType.DOUBLE_CLICK) {
				event.setCancelled(true);
			}
			if (rawslot == 1) {
				event.setCancelled(true);
			}
		} else if (view.getTopInventory().getType() == InventoryType.WORKBENCH) {
			String title = view.getTitle();
			title = Util.unhide(title);
			if (title.contains("[fragmentcombiner]")) {
				if (event.getClick() == ClickType.SHIFT_LEFT) {
					event.setCancelled(true);
				} else if (event.getClick() == ClickType.SHIFT_RIGHT) {
					event.setCancelled(true);
				}
				Inventory inv = view.getTopInventory();
				if (rawslot == 0) {
					event.setCancelled(true);
					if (event.getCurrentItem() != null) {
						Util.debug("Get random enchant.");
						int level = 1;
						ItemBuilder builder = new ItemBuilder(inv.getItem(3));
						String id = builder.getIdentifier(inv.getItem(3));
						if (id.contains("efrag")) {
							level = Integer.parseInt(id.split("_")[1]);
						}
						manager.craftRandomEnchantment((Player) event.getWhoClicked(), inv, level);
					}
				} else {
					Bukkit.getScheduler().scheduleSyncDelayedTask(main, new Runnable() {
						public void run() {
							manager.checkFragmentRecipe(inv);
						}
					}, 1);
				}
			}
		} else if (view.getTopInventory().getType() == InventoryType.ANVIL) {
			AnvilInventory anvil = (AnvilInventory) view.getTopInventory();

			ItemStack cursor = event.getCursor();
			ItemStack current = event.getCurrentItem();
			if (event.getClick() == ClickType.SHIFT_LEFT || event.getClick() == ClickType.SHIFT_RIGHT) {
				if (current != null) {
					if (current.getType() == Material.ENCHANTED_BOOK) {
						event.setCancelled(true);
					} else {
						ItemBuilder builder = new ItemBuilder();
						String identifier = builder.getIdentifier(current);
						if (identifier != null) {
							event.setCancelled(true);
						}
					}
				}
			}
			if (rawslot == 0) {
				if (cursor != null) {
					if (cursor.getType() == Material.ENCHANTED_BOOK) {
						event.setCancelled(true);
					} else {
						ItemBuilder builder = new ItemBuilder();
						String identifier = builder.getIdentifier(cursor);
						if (identifier != null) {
							event.setCancelled(true);
						}
					}
				}
			} else if (rawslot == 2) {
				if (current != null) {
					if (anvil.getItem(1) != null) {
						if (anvil.getItem(1).getType() == Material.ENCHANTED_BOOK) {
							event.setCancelled(true);
							event.setCursor(anvil.getItem(2));
							anvil.setItem(0, null);
							anvil.setItem(1, null);
						}
					}
				}
			}
			Bukkit.getScheduler().scheduleSyncDelayedTask(main, new Runnable() {
				public void run() {
					((Player) event.getWhoClicked()).setLevel(
							Main.getUserManager().getUser(event.getWhoClicked().getUniqueId().toString()).getLevel());
					ItemStack is = anvil.getItem(1);
					if (is != null) {
						if (is.getType() == Material.ENCHANTED_BOOK) {
							Util.debug("Trying to enchant item.");
							EnchantmentStorageMeta im = (EnchantmentStorageMeta) is.getItemMeta();
							ItemStack result = anvil.getItem(2);
							if (result != null) {
								for (Enchantment ench : im.getStoredEnchants().keySet()) {
									int level = im.getStoredEnchants().get(ench);
									result.addUnsafeEnchantment(ench, level);
								}
							}
						}
					}
				}
			}, 1);
		}
	}

	@EventHandler
	public void onInventoryDrag(InventoryDragEvent event) {
		InventoryView view = event.getView();
		String title = view.getTitle();
		title = Util.unhide(title);
		Util.debug("Title: " + title);
		if (title.contains("[fragmentcombiner]")) {
			Bukkit.getScheduler().scheduleSyncDelayedTask(main, new Runnable() {
				public void run() {
					manager.checkFragmentRecipe(view.getTopInventory());
				}
			}, 1);
		}
	}

	@EventHandler
	public void onInventoryClose(InventoryCloseEvent event) {
		Inventory inv = event.getInventory();
		if (inv.getType() == InventoryType.ENCHANTING) {
			inv.setItem(1, null);
			Bukkit.getScheduler().scheduleSyncDelayedTask(main, new Runnable() {
				public void run() {
					event.getPlayer().getInventory().remove(new ItemStack(Material.LAPIS_LAZULI));

				}
			}, 1);
		} else if (inv.getType() == InventoryType.WORKBENCH) {
			InventoryView view = event.getView();
			String title = Util.unhide(view.getTitle());
			if (title.contains("[fragmentcombiner]")) {
				for (int i = 1; i < inv.getSize(); i++) {
					ItemStack is = inv.getItem(i);
					if (is != null) {
						event.getPlayer().getInventory().addItem(is);
					}
				}
			}
		}
	}

	@EventHandler
	public void onPlayerInteract(PlayerInteractEvent event) {
		if (event.getAction() == Action.RIGHT_CLICK_BLOCK) {
			Block b = event.getClickedBlock();
			if (b.getType() == Material.ENCHANTING_TABLE) {
				event.setCancelled(true);
				if (manager != null) {
					manager.openEnchantFragmentCombiner(event.getPlayer());
				}
			}
		}
	}
}
