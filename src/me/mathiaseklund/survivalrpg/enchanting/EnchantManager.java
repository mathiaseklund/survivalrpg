package me.mathiaseklund.survivalrpg.enchanting;

import java.util.ArrayList;
import java.util.List;
import java.util.Random;

import org.bukkit.Bukkit;
import org.bukkit.ChatColor;
import org.bukkit.Material;
import org.bukkit.NamespacedKey;
import org.bukkit.enchantments.Enchantment;
import org.bukkit.entity.Player;
import org.bukkit.event.inventory.InventoryType;
import org.bukkit.inventory.Inventory;
import org.bukkit.inventory.ItemStack;
import org.bukkit.inventory.meta.EnchantmentStorageMeta;

import me.mathiaseklund.survivalrpg.Main;
import me.mathiaseklund.survivalrpg.items.ItemBuilder;
import me.mathiaseklund.survivalrpg.util.Util;

public class EnchantManager {

	Main main;

	public EnchantManager() {
		main = Main.getMain();
	}

	//
	// PUBLIC VOIDS
	//

	public void openEnchantFragmentCombiner(Player player) {
		Inventory inv = Bukkit.createInventory(player, InventoryType.WORKBENCH,
				Util.hide("[fragmentcombiner]") + ChatColor.translateAlternateColorCodes('&', "&bFragment Combiner"));
		player.openInventory(inv);
	}

	public void checkFragmentRecipe(Inventory inv) {
		List<ItemStack> ingredients = new ArrayList<ItemStack>();
		ingredients.add(inv.getItem(1));
		ingredients.add(inv.getItem(2));
		ingredients.add(inv.getItem(3));
		ingredients.add(inv.getItem(4));
		ingredients.add(inv.getItem(5));
		ingredients.add(inv.getItem(6));
		ingredients.add(inv.getItem(7));
		ingredients.add(inv.getItem(8));
		ingredients.add(inv.getItem(9));

		boolean foundIng = true;
		String ingId = "efrag";
		int level = 1;
		for (ItemStack is : ingredients) {
			if (is != null) {
				ItemBuilder builder = new ItemBuilder(is);
				String id = builder.getIdentifier(is);
				if (id != null) {
					// Util.debug("Found ID: " + id);
					if (!id.contains(ingId)) {
						foundIng = false;
						break;
					} else {
						level = Integer.parseInt(id.split("_")[1]);
					}
				} else {
					foundIng = false;
					break;
				}
			} else {
				foundIng = false;
				break;
			}
		}
		if (foundIng) {
			ItemStack result = Main.getItemManager().getCustomItem("renchant", level);
			inv.setItem(0, result);
		} else {
			inv.setItem(0, null);
		}
	}

	public void craftRandomEnchantment(Player player, Inventory inv, int level) {
		ItemStack book = getRandomEnchantmentBook(level);
		player.getInventory().addItem(book);
		inv.setItem(0, null);
		for (int i = 0; i < inv.getSize(); i++) {
			ItemStack is = inv.getItem(i);
			if (is != null) {
				if (is.getAmount() > 1) {
					is.setAmount(is.getAmount() - 1);
					inv.setItem(i, is);
				} else {
					inv.setItem(i, null);
				}
			}
		}
		checkFragmentRecipe(inv);
	}

	//
	// PRIVATE VOIDS
	//

	//
	// GETTERS
	//

	public ItemStack getRandomEnchantmentBook(int level) {
		ItemStack is = null;
		NamespacedKey type = getRandomEnchantment();
		/*
		 * ItemBuilder builder = new ItemBuilder();
		 * builder.setMaterial(Material.ENCHANTED_BOOK);
		 * builder.setDisplayName("&eEnchanted Book"); List<String> lore = new
		 * ArrayList<String>(); lore.add("&7" +
		 * (Util.capitalizeFirst(type.toLowerCase().replaceAll("_", " "))) + " " +
		 * Util.IntegerToRomanNumeral(level)); builder.setLore(lore);
		 * builder.setIdentifier("[enchantment" + type + "-" + level + "]"); is =
		 * builder.build();
		 */
		is = new ItemStack(Material.ENCHANTED_BOOK);
		EnchantmentStorageMeta im = (EnchantmentStorageMeta) is.getItemMeta();
		im.addStoredEnchant(Enchantment.getByKey(type), level, true);
		is.setItemMeta(im);
		Util.debug("Enchant: " + type);
		is.setAmount(1);
		return is;
	}

	public NamespacedKey getRandomEnchantment() {
		NamespacedKey enchant = null;
		Enchantment[] list = Enchantment.values();
		int amount = list.length;
		int generated = new Random().nextInt(amount - 1);
		for (int i = 0; i < amount; i++) {
			if (i == generated) {
				enchant = list[i].getKey();
				break;
			}
		}
		return enchant;
	}

}
