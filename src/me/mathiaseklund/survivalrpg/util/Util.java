package me.mathiaseklund.survivalrpg.util;

import org.bukkit.ChatColor;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;

import me.mathiaseklund.survivalrpg.Main;
import net.md_5.bungee.api.ChatMessageType;
import net.md_5.bungee.api.chat.TextComponent;

public class Util {

	/**
	 * Send a debug message to console.
	 * 
	 * @param message
	 */
	public static void debug(String message) {
		Main main = Main.getMain();
		if (main.getConfig().getBoolean("debug")) {
			System.out.println("[DEBUG SurvivalRPG] - " + message);
		}
	}

	/**
	 * Send message to recipient.
	 * 
	 * @param sender
	 * @param message
	 */
	public static void message(CommandSender sender, String message) {
		message = ChatColor.translateAlternateColorCodes('&', message);
		sender.sendMessage(message);
	}

	/**
	 * Send action bar message
	 * 
	 * @param player
	 * @param message
	 */
	public static void actionBarMessage(Player player, String message) {
		message = ChatColor.translateAlternateColorCodes('&', message);
		player.spigot().sendMessage(ChatMessageType.ACTION_BAR, new TextComponent(message));
	}

	/**
	 * Round decimal
	 * 
	 * @param value
	 *            original decimal
	 * @param places
	 *            amount of decimal places after whole number
	 * @return
	 */
	public static double round(double value, int places) {
		if (places < 0)
			throw new IllegalArgumentException();

		long factor = (long) Math.pow(10, places);
		value = value * factor;
		long tmp = Math.round(value);
		return (double) tmp / factor;
	}

	public static boolean isInteger(String s) {
		return isInteger(s, 10);
	}

	public static boolean isInteger(String s, int radix) {
		if (s.isEmpty())
			return false;
		for (int i = 0; i < s.length(); i++) {
			if (i == 0 && s.charAt(i) == '-') {
				if (s.length() == 1)
					return false;
				else
					continue;
			}
			if (Character.digit(s.charAt(i), radix) < 0)
				return false;
		}
		return true;
	}

	/**
	 * Turn a string invisible.
	 * 
	 * @param string
	 *            String to make Invisible
	 * @return Invisible String
	 */
	public static String hide(String string) {
		StringBuilder builder = new StringBuilder();

		for (char c : string.toCharArray()) {
			builder.append(ChatColor.COLOR_CHAR).append(c);
		}
		return builder.toString();
	}

	/**
	 * Turn a hidden string visible.
	 * 
	 * @param hidden
	 *            String that is Invisible
	 * @return Visible String
	 */
	public static String unhide(String hidden) {
		if (hidden != null) {
			return hidden.replaceAll(String.valueOf(ChatColor.COLOR_CHAR), "");
		} else {
			return null;
		}
	}

	public static String capitalizeFirst(String original) {
		String result = null;
		if (original == null || original.length() == 0) {
			return original;
		}

		if (original.contains(" ")) {
			for (String s : original.split(" ")) {
				if (result == null) {
					result = s.substring(0, 1).toUpperCase() + s.substring(1);
				} else {
					result = result + " " + s.substring(0, 1).toUpperCase() + s.substring(1);
				}
			}
		} else {
			result = original.substring(0, 1).toUpperCase() + original.substring(1);
		}
		return result;
	}

	public static String IntegerToRomanNumeral(int input) {
		if (input < 1 || input > 3999)
			return "Invalid Roman Number Value";
		String s = "";
		while (input >= 1000) {
			s += "M";
			input -= 1000;
		}
		while (input >= 900) {
			s += "CM";
			input -= 900;
		}
		while (input >= 500) {
			s += "D";
			input -= 500;
		}
		while (input >= 400) {
			s += "CD";
			input -= 400;
		}
		while (input >= 100) {
			s += "C";
			input -= 100;
		}
		while (input >= 90) {
			s += "XC";
			input -= 90;
		}
		while (input >= 50) {
			s += "L";
			input -= 50;
		}
		while (input >= 40) {
			s += "XL";
			input -= 40;
		}
		while (input >= 10) {
			s += "X";
			input -= 10;
		}
		while (input >= 9) {
			s += "IX";
			input -= 9;
		}
		while (input >= 5) {
			s += "V";
			input -= 5;
		}
		while (input >= 4) {
			s += "IV";
			input -= 4;
		}
		while (input >= 1) {
			s += "I";
			input -= 1;
		}
		return s;
	}
}
