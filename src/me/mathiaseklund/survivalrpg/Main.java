package me.mathiaseklund.survivalrpg;

import java.io.File;

import org.bukkit.Bukkit;
import org.bukkit.ChatColor;
import org.bukkit.entity.Player;
import org.bukkit.plugin.java.JavaPlugin;

import com.comphenix.protocol.ProtocolLibrary;
import com.comphenix.protocol.ProtocolManager;

import me.mathiaseklund.survivalrpg.anvil.AnvilListener;
import me.mathiaseklund.survivalrpg.blocks.BlockListener;
import me.mathiaseklund.survivalrpg.blocks.BlockManager;
import me.mathiaseklund.survivalrpg.chat.ChatListener;
import me.mathiaseklund.survivalrpg.chat.ChatManager;
import me.mathiaseklund.survivalrpg.commands.AttributePointsCommand;
import me.mathiaseklund.survivalrpg.commands.AttributesCommand;
import me.mathiaseklund.survivalrpg.commands.BlockDropCommand;
import me.mathiaseklund.survivalrpg.commands.CustomItemCommand;
import me.mathiaseklund.survivalrpg.commands.DexterityCommand;
import me.mathiaseklund.survivalrpg.commands.FeedCommand;
import me.mathiaseklund.survivalrpg.commands.FlyCommand;
import me.mathiaseklund.survivalrpg.commands.HealCommand;
import me.mathiaseklund.survivalrpg.commands.LevelCommand;
import me.mathiaseklund.survivalrpg.commands.MobDropCommand;
import me.mathiaseklund.survivalrpg.commands.StaminaCommand;
import me.mathiaseklund.survivalrpg.commands.StrengthCommand;
import me.mathiaseklund.survivalrpg.enchanting.EnchantListener;
import me.mathiaseklund.survivalrpg.enchanting.EnchantManager;
import me.mathiaseklund.survivalrpg.items.ItemListener;
import me.mathiaseklund.survivalrpg.items.ItemManager;
import me.mathiaseklund.survivalrpg.login.LoginListener;
import me.mathiaseklund.survivalrpg.mobs.MobListener;
import me.mathiaseklund.survivalrpg.mobs.MobManager;
import me.mathiaseklund.survivalrpg.time.TimeManager;
import me.mathiaseklund.survivalrpg.user.UserManager;
import me.vagdedes.mysql.database.SQL;
import net.coreprotect.CoreProtect;
import net.coreprotect.CoreProtectAPI;

public class Main extends JavaPlugin {

	static Main main;
	static UserManager usermanager;
	static ChatManager chatmanager;
	static MobManager mobmanager;
	static ItemManager itemmanager;
	static TimeManager timemanager;
	static BlockManager blockmanager;
	static EnchantManager enchantmanager;
	private ProtocolManager protocolManager;

	static CoreProtectAPI cpapi;

	public static Main getMain() {
		return main;
	}

	public void onEnable() {
		if (main == null) {
			main = this;
		}

		protocolManager = ProtocolLibrary.getProtocolManager();
		cpapi = CoreProtect.getInstance().getAPI();

		loadFiles();
		loadDatabase();

		if (usermanager == null) {
			usermanager = new UserManager();
		}
		if (chatmanager == null) {
			chatmanager = new ChatManager();
		}
		if (mobmanager == null) {
			mobmanager = new MobManager();
		}
		if (itemmanager == null) {
			itemmanager = new ItemManager();
		}
		if (timemanager == null) {
			timemanager = new TimeManager();
		}
		if (blockmanager == null) {
			blockmanager = new BlockManager();
		}
		if (enchantmanager == null) {
			enchantmanager = new EnchantManager();
		}
		registerListeners();
		registerCommands();
	}

	public ProtocolManager getProtocolManager() {
		return protocolManager;
	}

	public void onDisable() {
		logoutPlayers();
		mobmanager.unregisterEntities();

		Bukkit.getScheduler().cancelTasks(main);
	}

	void loadFiles() {
		this.saveDefaultConfig();

		File f = new File(getDataFolder(), "levels.txt");
		if (!f.exists()) {
			saveResource("levels.txt", true);
		}

		f = new File(getDataFolder() + "/drops/blocks.yml");
		if (!f.exists()) {
			saveResource("drops/blocks.yml", true);
		}

		f = new File(getDataFolder() + "/drops/entities.yml");
		if (!f.exists()) {
			saveResource("drops/entities.yml", true);
		}

		f = new File(getDataFolder(), "customitems.yml");
		if (!f.exists()) {
			saveResource("customitems.yml", true);
		}

		f = new File(getDataFolder() + "/recipes/shaped.yml");
		if (!f.exists()) {
			saveResource("recipes/shaped.yml", true);
		}

		f = new File(getDataFolder() + "/recipes/shapeless.yml");
		if (!f.exists()) {
			saveResource("recipes/shapeless.yml", true);
		}
	}

	void loadDatabase() {
		if (!SQL.tableExists("users")) {
			SQL.createTable("users",
					"id int AUTO_INCREMENT PRIMARY KEY, uuid varchar(255), level int DEFAULT 1, experience bigint DEFAULT 0,"
							+ "strength int default 3, stamina int default 10, dexterity int default 10,"
							+ " ap int default 0," + " created_on TIMESTAMP, online boolean default true");
		}
	}

	void registerListeners() {
		getServer().getPluginManager().registerEvents(new LoginListener(), this);
		getServer().getPluginManager().registerEvents(new ChatListener(), this);
		getServer().getPluginManager().registerEvents(new MobListener(), this);
		getServer().getPluginManager().registerEvents(new BlockListener(), this);
		getServer().getPluginManager().registerEvents(new AnvilListener(), this);
		getServer().getPluginManager().registerEvents(new EnchantListener(), this);
		getServer().getPluginManager().registerEvents(new ItemListener(), this);
	}

	void registerCommands() {
		getCommand("level").setExecutor(new LevelCommand());
		getCommand("strength").setExecutor(new StrengthCommand());
		getCommand("stamina").setExecutor(new StaminaCommand());
		getCommand("dexterity").setExecutor(new DexterityCommand());
		getCommand("attributes").setExecutor(new AttributesCommand());
		getCommand("attributepoints").setExecutor(new AttributePointsCommand());

		// ADMIN COMMANDS
		getCommand("feed").setExecutor(new FeedCommand());
		getCommand("heal").setExecutor(new HealCommand());
		getCommand("bdrop").setExecutor(new BlockDropCommand());
		getCommand("mdrop").setExecutor(new MobDropCommand());
		getCommand("fly").setExecutor(new FlyCommand());
		getCommand("ci").setExecutor(new CustomItemCommand());
	}

	void logoutPlayers() {
		for (Player p : Bukkit.getOnlinePlayers()) {
			p.kickPlayer(ChatColor.translateAlternateColorCodes('&', "&aServer Restarting"));
		}
	}

	public static CoreProtectAPI getCoreProtectAPI() {
		return cpapi;
	}

	public static UserManager getUserManager() {
		return usermanager;
	}

	public static BlockManager getBlockManager() {
		return blockmanager;
	}

	public static ChatManager getChatManager() {
		return chatmanager;
	}

	public static MobManager getMobManager() {
		return mobmanager;
	}

	public static ItemManager getItemManager() {
		return itemmanager;
	}

	public static EnchantManager getEnchantManager() {
		return enchantmanager;
	}

}
