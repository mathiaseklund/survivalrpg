package me.mathiaseklund.survivalrpg.commands;

import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;

import me.mathiaseklund.survivalrpg.Main;
import me.mathiaseklund.survivalrpg.user.User;
import me.mathiaseklund.survivalrpg.util.Util;

public class HealCommand implements CommandExecutor {

	@Override
	public boolean onCommand(CommandSender sender, Command arg1, String arg2, String[] args) {
		if (sender.isOp()) {
			if (args.length == 0) {
				if (sender instanceof Player) {
					User user = Main.getUserManager().getUser(((Player) sender).getUniqueId().toString());
					user.setHealth(user.getMaxHealth());
					Util.message(sender, "&6You've been &chealed&6.");
				}
			}
		}
		return false;
	}

}
