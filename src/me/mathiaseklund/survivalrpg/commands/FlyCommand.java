package me.mathiaseklund.survivalrpg.commands;

import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;

import me.mathiaseklund.survivalrpg.util.Util;

public class FlyCommand implements CommandExecutor {

	@Override
	public boolean onCommand(CommandSender sender, Command arg1, String arg2, String[] arg3) {
		if (sender instanceof Player) {
			if (sender.isOp()) {
				Player p = (Player) sender;
				boolean b = !p.getAllowFlight();
				p.setAllowFlight(b);
				p.setFlying(b);
				if (b) {
					Util.message(sender, "&6Fly has been turned &aON&6.");
				} else {
					Util.message(sender, "&6Fly has been turned &cOFF&6.");
				}
			}
		}
		return false;
	}

}
