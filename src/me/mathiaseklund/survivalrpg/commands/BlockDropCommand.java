package me.mathiaseklund.survivalrpg.commands;

import java.util.ArrayList;
import java.util.List;

import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;

import me.mathiaseklund.survivalrpg.Main;
import me.mathiaseklund.survivalrpg.blocks.BlockManager;
import me.mathiaseklund.survivalrpg.util.Util;

public class BlockDropCommand implements CommandExecutor {

	BlockManager manager = Main.getBlockManager();

	@Override
	public boolean onCommand(CommandSender sender, Command arg1, String arg2, String[] args) {
		if (sender.isOp()) {
			if (args.length == 0) {
				sendUsage(sender);
			} else if (args.length == 1) {
				String source = args[0].toUpperCase();
				Util.message(sender, "&a-- DROP LIST --");
				List<String> drops = manager.getDrops(source);
				for (String s : drops) {
					Util.message(sender, s);
				}
			} else if (args.length == 2) {
				if (args[0].equalsIgnoreCase("clear")) {
					String source = args[1].toUpperCase();
					manager.clearDrops(source);
					Util.message(sender, "&aCleared all drops from " + source);
				} else {
					sendUsage(sender);
				}
			} else if (args.length > 2) {
				String source = args[1].toUpperCase();
				List<String> arguments = new ArrayList<String>();
				for (int i = 2; i < args.length; i++) {
					arguments.add(args[i]);
				}

				if (args[0].equalsIgnoreCase("add")) {
					String dropString = null;
					boolean matfound = false;
					for (String s : arguments) {
						String arg = null;
						if (s.contains("material:")) {
							matfound = true;
							arg = s;
						} else if (s.contains("amount:")) {
							String str = s.split(":")[1];
							if (Util.isInteger(str)) {
								arg = s;
							}
						} else {
							arg = s;
						}

						if (arg != null) {
							if (dropString == null) {
								dropString = arg;
							} else {
								dropString = dropString + " " + arg;
							}
						}
					}
					if (matfound) {
						Util.message(sender, "&aAdded drop to " + source + ": " + dropString);
						manager.addDrop(source, dropString);
					} else {
						Util.message(sender, "&4ERROR:&7 Requires a material:[MATERIAL] argument.");
					}
				}
			}
		}
		return false;
	}

	void sendUsage(CommandSender sender) {
		usage(sender, "/bdrop [source]", "Display the drop data for a block.");
		usage(sender, "/bdrop clear [source]", "Clear the droplist for a block.");
		usage(sender, "/bdrop add [source] [args..]", "Add a drop option to a block.");
		usage(sender, "/bdrop remove [source] [material]", "Remove a drop option from a block");
	}

	void usage(CommandSender sender, String command, String desc) {
		Util.message(sender, "&e" + command + "&8 - &7" + desc);
	}

}
