package me.mathiaseklund.survivalrpg.commands;

import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;

import me.mathiaseklund.survivalrpg.Main;
import me.mathiaseklund.survivalrpg.levels.AttributeType;
import me.mathiaseklund.survivalrpg.user.User;
import me.mathiaseklund.survivalrpg.util.Util;

public class DexterityCommand implements CommandExecutor {

	@Override
	public boolean onCommand(CommandSender sender, Command arg1, String arg2, String[] args) {
		if (args.length == 0) {
			if (sender instanceof Player) {
				User user = Main.getUserManager().getUser(((Player) sender).getUniqueId().toString());
				int a = user.getDexterity();
				Util.message(sender, "&eYou currently have &a" + a + " Dexterity&e.");
			}
		} else if (args.length == 1) {
			if (sender instanceof Player) {
				if (Util.isInteger(args[0])) {
					User user = Main.getUserManager().getUser(((Player) sender).getUniqueId().toString());
					int p = Integer.parseInt(args[0]);
					int att = user.getAttributePoints();

					if (att >= p) {
						user.spendAttribute(p, AttributeType.DEX);
						Util.message(sender, "&aAdded &e" + p + "&a to your Dexterity Attribute!");
					} else {
						Util.message(sender, "&4ERROR:&7 Not enough Attribute Points for that.");
					}
				} else {
					Util.message(sender, "&4ERROR:&7 Argument needs to be an integer.");
				}
			}
		}
		return false;
	}

}
