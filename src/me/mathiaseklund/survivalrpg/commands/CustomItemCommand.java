package me.mathiaseklund.survivalrpg.commands;

import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;

import me.mathiaseklund.survivalrpg.Main;

public class CustomItemCommand implements CommandExecutor {

	@Override
	public boolean onCommand(CommandSender sender, Command arg1, String arg2, String[] args) {
		if (args.length == 0) {
			// /ci
		} else if (args.length > 0) {
			if (sender instanceof Player) {
				String itemId = args[0].toLowerCase();
				int amount = 1;
				if (args.length == 2) {
					amount = Integer.parseInt(args[1]);
				}
				Main.getItemManager().giveCustomItem((Player) sender, itemId, amount);
			} else {
				// Not run by player
			}

		}
		return false;
	}

}
