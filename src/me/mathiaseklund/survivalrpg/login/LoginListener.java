package me.mathiaseklund.survivalrpg.login;

import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.player.PlayerJoinEvent;
import org.bukkit.event.player.PlayerLoginEvent;
import org.bukkit.event.player.PlayerLoginEvent.Result;
import org.bukkit.event.player.PlayerQuitEvent;

import me.mathiaseklund.survivalrpg.Main;
import me.mathiaseklund.survivalrpg.user.User;
import me.mathiaseklund.survivalrpg.user.UserManager;

public class LoginListener implements Listener {

	Main main = Main.getMain();

	@EventHandler
	public void onPlayerLogin(PlayerLoginEvent event) {
		Player player = event.getPlayer();
		String uuid = player.getUniqueId().toString();
		// TODO check banned status
		if (Login.canPlay(uuid)) {
			User user = new User(uuid);
			UserManager manager = Main.getUserManager();
			manager.newUser(user);
		} else {
			event.disallow(Result.KICK_BANNED, "You are not allowed to play.");
			// TODO Get reason
		}
	}

	@EventHandler
	public void onPlayerJoin(PlayerJoinEvent event) {
		//User user = Main.getUserManager().getUser(event.getPlayer().getUniqueId().toString());
		//user.setHealth(user.getMaxHealth());
	}

	@EventHandler
	public void onPlayerQuit(PlayerQuitEvent event) {
		Player player = event.getPlayer();
		String uuid = player.getUniqueId().toString();

		Login.logout(uuid);

	}
}
