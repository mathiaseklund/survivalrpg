package me.mathiaseklund.survivalrpg.login;

import me.mathiaseklund.survivalrpg.Main;
import me.mathiaseklund.survivalrpg.user.User;
import me.mathiaseklund.survivalrpg.user.UserManager;

public class Login {

	/**
	 * Check if user is banned.
	 * 
	 * @param uuid
	 * @return
	 */
	public static boolean isBanned(String uuid) {
		// TODO get ban time and reason, in different function.
		return false;
	}

	/**
	 * Check if user can play on the server.
	 * 
	 * @param uuid
	 * @return
	 */
	public static boolean canPlay(String uuid) {
		boolean b = true;
		if (isBanned(uuid)) {
			b = false;
		}
		return b;
	}

	public static void logout(String uuid) {
		UserManager manager = Main.getUserManager();
		User user = manager.getUser(uuid);
		if (user != null) {
			user.setOnline(false);
			manager.logout(user);
		}
	}
}
