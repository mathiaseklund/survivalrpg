package me.mathiaseklund.survivalrpg.mobs;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Random;

import org.bukkit.Material;
import org.bukkit.configuration.file.FileConfiguration;
import org.bukkit.configuration.file.YamlConfiguration;
import org.bukkit.entity.Entity;
import org.bukkit.entity.EntityType;
import org.bukkit.entity.LivingEntity;
import org.bukkit.entity.Player;
import org.bukkit.entity.Projectile;
import org.bukkit.inventory.ItemStack;
import org.bukkit.inventory.meta.ItemMeta;
import org.bukkit.metadata.MetadataValue;

import me.mathiaseklund.survivalrpg.Main;
import me.mathiaseklund.survivalrpg.util.Util;

public class MobManager {

	Main main = Main.getMain();

	File df;
	FileConfiguration dropsData;

	HashMap<Integer, Entity> entities;

	public MobManager() {
		main = Main.getMain();
		entities = new HashMap<Integer, Entity>();
		loadDrops();
	}

	// PRIVATE VOIDS

	private void loadDrops() {
		df = new File(main.getDataFolder() + "/drops/entities.yml");
		dropsData = YamlConfiguration.loadConfiguration(df);

	}

	private void saveDrops() {
		try {
			dropsData.save(df);
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

	// PUBLIC VOIDS

	public void registerEntity(Entity ent) {
		entities.put(ent.getEntityId(), ent);
	}

	public void unregisterEntity(int entityid) {
		entities.remove(entityid);
	}

	public void unregisterEntities() {
		for (Entity ent : entities.values()) {
			ent.remove();
		}
		entities.clear();
	}

	public void removeDrop(String source, String drop) {
		List<String> drops = getDrops(source);
		if (drops == null) {
			drops = new ArrayList<String>();
		}

		for (int i = 0; i < drops.size(); i++) {
			String dropString = drops.get(i);
			if (dropString.contains(drop)) {
				drops.remove(dropString);
				break;
			}
		}

		setDrops(source, drops);
	}

	public void setDrops(String source, List<String> drops) {
		dropsData.set(source, drops);
		saveDrops();
		loadDrops();
	}

	public void clearDrops(String source) {
		dropsData.set(source, null);
		saveDrops();
	}

	public void addDrop(String source, String dropString) {
		List<String> drops = getDrops(source);
		if (drops == null) {
			drops = new ArrayList<String>();
		}

		drops.add(dropString);

		setDrops(source, drops);
	}

	public void spawnDamageHologram(Player player, double damage) {// (Location location, double damage) {
		/*
		 * ArmorStand as = (ArmorStand) location.getWorld().spawnEntity(location.add(0,
		 * 2, 0), EntityType.ARMOR_STAND);
		 * 
		 * as.setGravity(false); as.setCanPickupItems(false);
		 * as.setCustomName(ChatColor.translateAlternateColorCodes('&', "6c" + damage));
		 * as.setCustomNameVisible(true); as.setVisible(false);
		 * 
		 * registerEntity(as);
		 * 
		 * Bukkit.getScheduler().scheduleSyncDelayedTask(main, new Runnable() { public
		 * void run() { unregisterEntity(as.getEntityId()); as.remove(); } }, 20);
		 */

		Util.actionBarMessage(player, "&c+" + damage + " Damage");
	}

	// DOUBLES

	public double getBaseDamage(Entity entity) {
		int strength = getStrength(entity);

		double dps = main.getConfig().getDouble("damage-per-strength");
		return (dps * strength);
	}

	public double getMaxHealth(int stamina) {
		double hps = main.getConfig().getDouble("health-per-stamina");
		return (hps * stamina);
	}

	// INTEGERS

	public int getStrength(Entity ent) {
		int strength = 3;

		if (ent.hasMetadata("strength")) {
			Util.debug("Found entity metdata for strength");
			for (MetadataValue meta : ent.getMetadata("strength")) {
				int value = meta.asInt();
				Util.debug("Value: " + value);
				if (value > 0) {
					strength = value;
					break;
				}
			}
		}
		return strength;
	}

	public int getStamina(Entity ent) {
		int stamina = 3;

		if (ent.hasMetadata("stamina")) {
			Util.debug("Found entity metdata for stamina");
			for (MetadataValue meta : ent.getMetadata("stamina")) {
				int value = meta.asInt();
				Util.debug("Value: " + value);
				if (value > 0) {
					stamina = value;
					break;
				}
			}
		}
		return stamina;
	}

	public int getDexterity(Entity ent) {
		int dexterity = 3;

		if (ent.hasMetadata("dexterity")) {
			Util.debug("Found entity metdata for dexterity");
			for (MetadataValue meta : ent.getMetadata("dexterity")) {
				int value = meta.asInt();
				Util.debug("Value: " + value);
				if (value > 0) {
					dexterity = value;
					break;
				}
			}
		}
		return dexterity;
	}

	public int getLevel(Entity ent) {
		int level = 1;

		if (ent.hasMetadata("level")) {
			Util.debug("Found entity metdata for level");
			for (MetadataValue meta : ent.getMetadata("level")) {
				int value = meta.asInt();
				Util.debug("Value: " + value);
				if (value > 0) {
					level = value;
					break;
				}
			}
		}
		return level;
	}

	// LISTS

	public List<ItemStack> getDrops(Entity ent) {
		Util.debug(ent.getType().toString());
		List<ItemStack> drops = new ArrayList<ItemStack>();
		List<String> list = getDrops(ent.getType().toString());
		if (list != null) {
			if (!list.isEmpty()) {
				for (String dropString : list) {
					String material = null;
					String custom = null;
					int amount = 1;
					double chance = 1;
					// TODO add a level requirement for the drop
					for (String part : dropString.split(" ")) {
						String data = part.split(":")[1];
						if (part.contains("material:")) {
							material = data.toUpperCase();
						} else if (part.contains("amount:")) {
							if (data.contains("-")) {
								int min = Integer.parseInt(data.split("-")[0]);
								int max = Integer.parseInt(data.split("-")[1]);
								amount = new Random().nextInt((max - min) + 1) + min;
							} else {
								amount = Integer.parseInt(data);
							}
						} else if (part.contains("chance:")) {
							chance = Double.parseDouble(data);
						} else if (part.contains("custom:")) {
							custom = data;
						}
					}
					if (material != null) {
						ItemStack is = null;
						ItemMeta im = null;
						boolean drop = true;
						if (chance < 1) {
							double rolled = new Random().nextDouble();
							Util.debug("Material " + material + " rolled: " + rolled);
							if (rolled > chance) {
								drop = false;
							}
						}
						if (drop) {
							is = new ItemStack(Material.getMaterial(material));
							im = is.getItemMeta();
							is.setAmount(amount);
							is.setItemMeta(im);
						}

						if (is != null) {
							drops.add(is);
						}
					} else {
						if (custom != null) {
							ItemStack is = Main.getItemManager().getCustomItem(custom, amount);

							boolean drop = true;
							if (chance < 1) {
								double rolled = new Random().nextDouble();
								Util.debug("Material " + material + " rolled: " + rolled);
								if (rolled > chance) {
									drop = false;
								}
							}

							if (drop) {
								drops.add(is);
							}
						}
					}
				}
			}
		}
		return drops;
	}

	public List<String> getDrops(String source) {
		return dropsData.getStringList(source);
	}

	public List<Integer> getAttributes(int level) {
		List<Integer> list = new ArrayList<Integer>();
		int stamina = 10;
		int strength = 3;
		int dexterity = 10;
		int apl = main.getConfig().getInt("ap-per-level");
		int ap = ((level * apl) - apl) * 2;
		Util.debug("Attribute Amount: " + ap);

		stamina += (ap / 3);
		strength += (ap / 3);
		dexterity += (ap / 3);

		list.add(stamina);
		list.add(strength);
		list.add(dexterity);
		return list;
	}

	// ITEMSTACKS

	public ItemStack getItemInMainHand(Entity entity) {
		ItemStack hand = null;
		LivingEntity lent = null;
		if (entity instanceof LivingEntity) {
			lent = (LivingEntity) entity;
		} else {
			if (entity instanceof Projectile) {
				Projectile proj = (Projectile) entity;
				lent = (LivingEntity) proj.getShooter();
			}
		}
		if (lent != null) {
			hand = lent.getEquipment().getItemInMainHand();
		}
		return hand;
	}
}
