package me.mathiaseklund.survivalrpg.mobs;

import java.util.List;

import org.bukkit.Bukkit;
import org.bukkit.Location;
import org.bukkit.attribute.Attribute;
import org.bukkit.entity.Entity;
import org.bukkit.entity.EntityType;
import org.bukkit.entity.LivingEntity;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.EventPriority;
import org.bukkit.event.Listener;
import org.bukkit.event.entity.CreatureSpawnEvent;
import org.bukkit.event.entity.EntityDamageByEntityEvent;
import org.bukkit.event.entity.EntityDamageEvent;
import org.bukkit.event.entity.EntityDeathEvent;
import org.bukkit.event.player.PlayerArmorStandManipulateEvent;
import org.bukkit.inventory.ItemStack;
import org.bukkit.metadata.FixedMetadataValue;

import me.mathiaseklund.survivalrpg.Main;
import me.mathiaseklund.survivalrpg.levels.ExperienceAPI;
import me.mathiaseklund.survivalrpg.user.User;
import me.mathiaseklund.survivalrpg.util.Util;
import net.md_5.bungee.api.ChatColor;

public class MobListener implements Listener {

	Main main = Main.getMain();
	MobManager manager = Main.getMobManager();

	// TODO attack speed for both mobs and player

	@EventHandler(priority = EventPriority.MONITOR)
	public void onCreatureSpawn(CreatureSpawnEvent event) {
		if (!event.isCancelled()) {
			EntityType type = event.getEntityType();
			if (type != EntityType.ARMOR_STAND) {
				Location spawnLocation = event.getLocation();
				Location center = spawnLocation.getWorld().getSpawnLocation();

				int range = main.getConfig().getInt("mob-range");
				double distance = center.distance(spawnLocation);
				double mob_level = distance / range;
				mob_level += 1;
				int level = (int) Math.floor(mob_level);
				Util.debug(event.getEntity().getType().toString() + " Spawned " + distance
						+ " blocks away from the Center.");
				Util.debug("Mob will be level: " + level);

				Entity ent = event.getEntity();
				manager.registerEntity(ent);

				List<Integer> attributes = manager.getAttributes(level);
				int stamina = attributes.get(0);
				int strength = attributes.get(1);
				int dexterity = attributes.get(2);

				Util.debug("Mob Stamina: " + stamina);
				Util.debug("Mob Strength: " + strength);
				Util.debug("Mob Dexterity: " + dexterity);

				double maxhealth = manager.getMaxHealth(stamina);

				LivingEntity lent = (LivingEntity) ent;
				lent.getEquipment().clear();
				lent.getAttribute(Attribute.GENERIC_MAX_HEALTH).setBaseValue(maxhealth);
				lent.setHealth(lent.getAttribute(Attribute.GENERIC_MAX_HEALTH).getBaseValue());

				ent.setMetadata("stamina", new FixedMetadataValue(main, stamina));
				ent.setMetadata("strength", new FixedMetadataValue(main, strength));
				ent.setMetadata("dexterity", new FixedMetadataValue(main, dexterity));
				ent.setMetadata("level", new FixedMetadataValue(main, level));

				String prefix = "&e[" + level + "] &f";
				String suffix = " &c[" + maxhealth + "]";

				ent.setCustomName(ChatColor.translateAlternateColorCodes('&', prefix + ent.getName() + suffix));
				// ent.setCustomNameVisible(true);
			}

		}
	}

	@EventHandler(priority = EventPriority.MONITOR)
	public void onEntityDeath(EntityDeathEvent event) {
		event.setDroppedExp(0);
		Entity ent = event.getEntity();
		Player killer = event.getEntity().getKiller();
		if (!(ent instanceof Player)) {
			int id = ent.getEntityId();
			manager.unregisterEntity(id);
			int level = 0;
			if (ent.hasMetadata("level")) {
				level = ent.getMetadata("level").get(0).asInt();
			}
			if (level > 0) {
				if (killer != null) {
					Util.debug("Player killed a mob.");
					User user = Main.getUserManager().getUser(killer.getUniqueId().toString());
					int plevel = user.getLevel();
					long experience = ExperienceAPI.getExperienceGainedFromKill(plevel, level);
					user.giveExperience(experience);
				}
			}

			List<ItemStack> drops = manager.getDrops(ent);
			if (drops != null) {
				if (!drops.isEmpty()) {
					for (ItemStack is : drops) {
						ent.getWorld().dropItemNaturally(ent.getLocation(), is);
					}
				}
			}

			event.setDroppedExp(0);
			event.getDrops().clear();
		}
	}

	@EventHandler
	public void onEntityDamageByEntity(EntityDamageByEntityEvent event) {
		double vanillaDamage = event.getDamage();
		Util.debug("Vanilla Damage: " + vanillaDamage);
		event.setDamage(0);
		double basedamage = 0;
		double itemdamage = vanillaDamage;
		if (event.getDamager() instanceof Player) {
			// Player attacks
			Player dmger = (Player) event.getDamager();
			User damager = Main.getUserManager().getUser(dmger.getUniqueId().toString());
			basedamage = damager.getBaseDamage();

			ItemStack hand = dmger.getInventory().getItemInMainHand();
			if (hand != null) {
				// itemdamage = Main.getItemManager().getDamage(hand);

			}
		} else {
			// Entity attacks
			Entity entity = event.getDamager();
			basedamage = manager.getBaseDamage(entity);

			ItemStack hand = manager.getItemInMainHand(entity);
			if (hand != null) {
				// itemdamage = Main.getItemManager().getDamage(hand);
			}
		}
		basedamage = Util.round(basedamage, 1);
		itemdamage = Util.round(itemdamage, 1);
		double damage = basedamage + itemdamage;
		Util.debug("Base Damage: " + basedamage);
		Util.debug("Item Damage: " + itemdamage);
		Util.debug("Total Damage: " + damage);
		event.setDamage(damage);
		if (event.getDamager() instanceof Player) {
			manager.spawnDamageHologram((Player) event.getDamager(), damage);
		}
	}

	@EventHandler(priority = EventPriority.MONITOR)
	public void onEntityDamage(EntityDamageEvent event) {
		if (!event.isCancelled()) {
			if (!(event.getEntity() instanceof Player)) {
				Bukkit.getScheduler().scheduleSyncDelayedTask(main, new Runnable() {
					public void run() {
						if (event.getEntity() instanceof LivingEntity) {
							LivingEntity ent = (LivingEntity) event.getEntity();
							String prefix, suffix;
							prefix = "&e[" + manager.getLevel(ent) + "] &f";
							suffix = " &c[" + Util.round(ent.getHealth(), 1) + "]";
							ent.setCustomName(null);
							ent.setCustomName(
									ChatColor.translateAlternateColorCodes('&', prefix + ent.getName() + suffix));
						}
					}
				}, 1);

			}
		}
	}

	@EventHandler
	public void onArmorStandManipulate(PlayerArmorStandManipulateEvent e) {
		if (!e.getRightClicked().isVisible()) {
			e.setCancelled(true);
		}
	}
}
