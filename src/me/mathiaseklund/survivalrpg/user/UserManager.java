package me.mathiaseklund.survivalrpg.user;

import java.util.HashMap;

import me.mathiaseklund.survivalrpg.Main;

public class UserManager {

	Main main;
	HashMap<String, User> online;

	public UserManager() {
		main = Main.getMain();
		online = new HashMap<String, User>();
	}

	public void newUser(User user) {
		online.put(user.getUUID(), user);
	}

	public User getUser(String uuid) {
		if (online.containsKey(uuid)) {
			return online.get(uuid);
		} else {
			return null;
		}
	}

	public void logout(User user) {
		online.remove(user.getUUID());
	}
}
