package me.mathiaseklund.survivalrpg.user;

import java.util.UUID;

import org.bukkit.Bukkit;
import org.bukkit.Sound;
import org.bukkit.attribute.Attribute;
import org.bukkit.entity.Player;

import me.mathiaseklund.survivalrpg.Main;
import me.mathiaseklund.survivalrpg.levels.AttributeType;
import me.mathiaseklund.survivalrpg.levels.ExperienceAPI;
import me.mathiaseklund.survivalrpg.util.Util;
import me.vagdedes.mysql.database.SQL;

public class User {

	Main main;

	String uuid;
	String createdAt;

	int userId = -1;
	int level = -1;
	int strength = -1;
	int dexterity = -1;
	int stamina = -1;
	int attributepoints = -1;

	long experience = -1;

	double maxhealth = -1;

	boolean saving = false;
	boolean online;

	public User(String uuid) {
		this.main = Main.getMain();
		this.uuid = uuid;
		this.online = true;
		load();
		// TODO get data from database
	}

	// PLAYER

	Player getPlayer() {
		Player p = Bukkit.getPlayer(UUID.fromString(getUUID()));
		return p;
	}

	// STRINGS

	String getUUID() {
		return this.uuid;
	}

	String getCreatedAt() {
		// TODO
		Object o = SQL.get("created_on", "uuid", "=", getUUID(), "users");
		createdAt = o.toString();
		return createdAt;
	}

	// INTS

	int getUserId() {
		if (this.userId < 0) {
			Object o = SQL.get("id", "uuid", "=", getUUID(), "users");
			if (o instanceof Integer) {
				userId = (int) o;
				return userId;
			} else {
				return userId;
			}
		} else {
			return userId;
		}
	}

	public int getLevel() {
		if (this.level < 0) {
			Object o = SQL.get("level", "uuid", "=", getUUID(), "users");
			if (o instanceof Integer) {
				level = (int) o;
				return level;
			} else {
				return 1;
			}
		} else {
			return level;
		}
	}

	public int getStrength() {
		if (this.strength < 0) {
			Object o = SQL.get("strength", "id", "=", getUserId() + "", "users");
			if (o instanceof Integer) {
				this.strength = (int) o;
				return strength;
			} else {
				return 0;
			}
		} else {
			return strength;
		}
	}

	public int getStamina() {
		if (this.stamina < 0) {
			Object o = SQL.get("stamina", "id", "=", getUserId() + "", "users");
			if (o instanceof Integer) {
				this.stamina = (int) o;
				return stamina;
			} else {
				return 0;
			}
		} else {
			return stamina;
		}
	}

	public int getDexterity() {
		if (this.dexterity < 0) {
			Object o = SQL.get("dexterity", "id", "=", getUserId() + "", "users");
			if (o instanceof Integer) {
				this.dexterity = (int) o;
				return dexterity;
			} else {
				return 0;
			}
		} else {
			return dexterity;
		}
	}

	public int getAttributePoints() {
		if (this.attributepoints < 0) {
			Object o = SQL.get("ap", "id", "=", getUserId() + "", "users");
			if (o instanceof Integer) {
				this.attributepoints = (int) o;
				return attributepoints;
			} else {
				return 0;
			}
		} else {
			return this.attributepoints;
		}
	}

	// LONGS

	public long getExperience() {
		if (this.experience < 0) {
			Object o = SQL.get("experience", "uuid", "=", getUUID(), "users");
			if (o instanceof Long) {
				experience = (long) o;
				return experience;
			} else {
				return 0;
			}
		} else {
			return experience;
		}
	}

	public long getExperienceRequiredToLevel() {
		return ExperienceAPI.getRequiredExp(getLevel());
	}

	public long getExperienceRemainingToLevel() {
		return ExperienceAPI.getRemainingExperience(getLevel(), getExperience());
	}

	// DOUBLES

	public double getHealth() {

		Player player = Bukkit.getPlayer(UUID.fromString(getUUID()));
		if (player != null) {
			return player.getHealth();
		} else {
			return 0;
		}
	}

	public double getMaxHealth() {
		int stamina = getStamina();
		double hps = main.getConfig().getInt("health-per-stamina");
		this.maxhealth = hps * stamina;

		Player player = Bukkit.getPlayer(UUID.fromString(getUUID()));
		if (player != null) {
			player.getAttribute(Attribute.GENERIC_MAX_HEALTH).setBaseValue(this.maxhealth);
		}
		return this.maxhealth;
	}

	public double getBaseDamage() {
		int strength = getStrength();
		double dps = main.getConfig().getDouble("damage-per-strength");
		return (dps * strength);
	}

	// BOOLEANS

	public boolean isOnline() {
		return online;
	}

	// VOIDS

	void load() {
		Bukkit.getScheduler().runTaskAsynchronously(main, new Runnable() {
			public void run() {
				if (SQL.exists("uuid", getUUID(), "users")) {
					Util.debug("User found in database.");

					// Get data from database
					getUserId();
					getPlayer().setLevel(getLevel());
					getExperience();
					getCreatedAt();
					setOnline(true);
					getMaxHealth();
					getHealth();

				} else {
					Util.debug("User not found in database.");
					create();
				}
			}
		});
	}

	void create() {
		SQL.insertData("uuid", "'" + getUUID() + "'", "users");
	}

	void save() {
		// TODO save player data to database.
		Util.debug("Saving player data for UserId: " + getUserId());

		SQL.set("level", getLevel(), "id", "=", getUserId() + "", "users");
		SQL.set("experience", getExperience(), "id", "=", getUserId() + "", "users");
		SQL.set("ap", getAttributePoints(), "id", "=", getUserId() + "", "users");
		SQL.set("strength", getStrength(), "id", "=", getUserId() + "", "users");
		SQL.set("stamina", getStamina(), "id", "=", getUserId() + "", "users");
		SQL.set("dexterity", getDexterity(), "id", "=", getUserId() + "", "users");
	}

	void trySave() {
		if (!saving) {
			saving = true;
			Bukkit.getScheduler().scheduleSyncDelayedTask(main, new Runnable() {
				public void run() {
					saving = false;
					save();
				}
			}, 20 * 30);
		}
	}

	public void giveExperience(long experience) {
		Player player = getPlayer();
		if (player != null) {
			Util.actionBarMessage(player, "&a+" + experience + " &6Experience");
		}
		this.experience += experience;
		long remaining = getExperienceRemainingToLevel();
		long required = getExperienceRequiredToLevel();
		double percentage = ((this.experience * 100) / required);

		Util.debug("percentage: " + percentage);
		if (remaining <= 0) {
			long newexp = this.experience - required;
			this.experience = newexp;
			percentage = ((this.experience * 100) / required) / 100;
			levelUp();
			giveLevelUpAP();
		}
		float percent = (float) (percentage / 100);
		getPlayer().setExp(percent);
		trySave();
	}

	public void levelUp() {
		Player player = getPlayer();
		if (player != null) {
			Util.actionBarMessage(player, "&l&6Level Up!");
			player.playSound(player.getLocation(), Sound.ENTITY_PLAYER_LEVELUP, 1, 1);
		}
		this.level = this.level + 1;
		getPlayer().setLevel(this.level);
	}

	public void giveLevelUpAP() {
		Player player = getPlayer();
		int ap = getAttributePoints();
		int reward = main.getConfig().getInt("ap-per-level");
		int newap = ap + reward;
		setAttributePoints(newap);
		if (player != null) {
			Util.message(player, "&a+" + reward + " &6Attribute Points");
		}
	}

	public void setAttributePoints(int ap) {
		this.attributepoints = ap;
		trySave();
	}

	public void setOnline(boolean b) {
		int i = 0;
		if (b) {
			i = 1;
		}
		SQL.set("online", i, "id", "=", getUserId() + "", "users");
		online = b;
	}

	public void setHealth(double health) {
		double maxhealth = getMaxHealth();
		if (health > maxhealth) {
			health = maxhealth;
		}
		Player player = Bukkit.getPlayer(UUID.fromString(getUUID()));
		if (player != null) {
			player.setHealth(health);
		}
	}

	public void spendAttribute(int points, AttributeType type) {
		int atts = getAttributePoints();
		if (atts >= points) {
			if (type == AttributeType.STR) {
				setStrength(getStrength() + points);
				setAttributePoints(atts - points);
			} else if (type == AttributeType.STA) {
				setStamina(getStamina() + points);
				setAttributePoints(atts - points);
			} else if (type == AttributeType.DEX) {
				setDexterity(getDexterity() + points);
				setAttributePoints(atts - points);
			}
		}
	}

	public void setStrength(int str) {
		this.strength = str;
		trySave();
	}

	public void setStamina(int sta) {
		this.stamina = sta;
		getMaxHealth();
		trySave();
	}

	public void setDexterity(int dex) {
		this.dexterity = dex;
		trySave();
	}
}
