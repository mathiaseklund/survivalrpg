package me.mathiaseklund.survivalrpg.time;

import java.util.Calendar;

import org.bukkit.Bukkit;
import org.bukkit.World;
import org.bukkit.entity.Player;

import me.mathiaseklund.survivalrpg.Main;
import me.mathiaseklund.survivalrpg.util.Util;

public class TimeManager {

	Main main;

	public TimeManager() {
		this.main = Main.getMain();
		setRealTime();
	}

	void setRealTime() {
		@SuppressWarnings("deprecation")
		int hour = Calendar.getInstance().getTime().getHours();
		for (World w : Bukkit.getWorlds()) {
			boolean night = false;

			if (hour >= 20) {
				night = true;
			} else if (hour <= 4) {
				night = true;
			}

			long prev = w.getTime();

			if (night) {
				if (prev != 18000) {
					w.setTime(18000);
					for (Player p : w.getPlayers()) {
						Util.message(p, "&cA new Night has begun!");
					}
				}
			} else {
				if (prev != 1000) {
					w.setTime(1000);
					for (Player p : w.getPlayers()) {
						Util.message(p, "&aA new Day has begun!");
					}
				}
			}
		}

		Bukkit.getScheduler().scheduleSyncDelayedTask(main, new Runnable() {
			public void run() {
				setRealTime();
			}
		}, 10 * 20);
	}
}
