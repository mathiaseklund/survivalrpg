package me.mathiaseklund.survivalrpg.blocks;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.Random;

import org.bukkit.Material;
import org.bukkit.block.Block;
import org.bukkit.configuration.file.FileConfiguration;
import org.bukkit.configuration.file.YamlConfiguration;
import org.bukkit.enchantments.Enchantment;
import org.bukkit.entity.Player;
import org.bukkit.inventory.ItemStack;
import org.bukkit.inventory.meta.ItemMeta;

import me.mathiaseklund.survivalrpg.Main;
import me.mathiaseklund.survivalrpg.util.Util;
import net.coreprotect.CoreProtectAPI;

public class BlockManager {

	Main main;

	CoreProtectAPI api;

	File f;
	FileConfiguration data;

	// CONSTRUCTOR
	public BlockManager() {
		main = Main.getMain();
		api = Main.getCoreProtectAPI();
		load();
	}

	// LISTS
	public List<ItemStack> getDrops(Block block, ItemStack tool) {
		Util.debug(block.getType().toString());
		List<ItemStack> drops = new ArrayList<ItemStack>();
		List<String> list = getDrops(block.getType().toString());
		if (list != null) {
			if (!list.isEmpty()) {
				for (String dropString : list) {
					String material = null;
					int amount = 1;
					double chance = 1;
					for (String part : dropString.split(" ")) {
						String data = part.split(":")[1];
						if (part.contains("material:")) {
							material = data.toUpperCase();
						} else if (part.contains("amount:")) {
							amount = Integer.parseInt(data);
						} else if (part.contains("chance:")) {
							chance = Double.parseDouble(data);
						}
					}
					if (material != null) {
						ItemStack is = null;
						ItemMeta im = null;
						boolean drop = true;
						if (chance < 1) {
							double rolled = new Random().nextDouble();
							Util.debug("Material " + material + " rolled: " + rolled);
							if (rolled > chance) {
								drop = false;
							}
						}
						if (drop) {
							// TODO int silktouch = 0;
							int fortunelevel = 0;
							if (tool != null) {
								for (Enchantment ench : tool.getEnchantments().keySet()) {
									Util.debug("Enchantment Found: " + ench.getKey());
									int level = tool.getEnchantments().get(ench);
									if (ench.getKey().getKey().equalsIgnoreCase("fortune")) {
										Util.debug("Found fortune");
										fortunelevel = level;
									}
								}
							}

							Util.debug("Fortune Level: " + fortunelevel);
							amount = amount * getDropCount(fortunelevel, new Random());
							is = new ItemStack(Material.getMaterial(material));
							im = is.getItemMeta();
							is.setAmount(amount);
							is.setItemMeta(im);
						}

						if (is != null) {
							drops.add(is);
						}
					}
				}
			}
		}
		if (drops.isEmpty()) {
			// int silktouch = 0;
			int fortunelevel = 0;
			if (tool != null) {
				for (Enchantment ench : tool.getEnchantments().keySet()) {
					Util.debug("Enchantment Found: " + ench.getKey());
					int level = tool.getEnchantments().get(ench);
					if (ench.getKey().getKey().equalsIgnoreCase("fortune")) {
						Util.debug("Found fortune");
						fortunelevel = level;
					}
				}
			}
			Util.debug("Fortune Level: " + fortunelevel);
			for (ItemStack is : block.getDrops()) {
				is.setAmount(is.getAmount() * getDropCount(fortunelevel, new Random()));
				drops.add(is);
			}
		}
		return drops;
	}

	public List<String> getDrops(String source) {
		return data.getStringList(source);
	}

	// INTEGERS

	public int getDropCount(int fortunelevel, Random random) {
		int times = 1;
		if (fortunelevel > 0) {
			times = random.nextInt(fortunelevel + 2);
			Util.debug("Times: " + times);
			if (times < 1) {
				times = 1;
			}
		}
		return times;
	}

	// BOOLEANS

	public boolean hasPlaced(Player player, Block block) {
		if (api != null) {
			return api.hasPlaced(player.getName(), block, 604800, 0);
		} else {
			Util.debug("CoreProtect API not found");
			return false;
		}
	}

	// PRIVATE VOIDS
	private void load() {
		f = new File(main.getDataFolder() + "/drops/blocks.yml");
		data = YamlConfiguration.loadConfiguration(f);
	}

	private void save() {
		try {
			data.save(f);
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

	// PUBLIC VOIDS

	public void removeDrop(String source, String drop) {
		List<String> drops = getDrops(source);
		if (drops == null) {
			drops = new ArrayList<String>();
		}

		for (int i = 0; i < drops.size(); i++) {
			String dropString = drops.get(i);
			if (dropString.contains(drop)) {
				drops.remove(dropString);
				break;
			}
		}

		setDrops(source, drops);
	}

	public void setDrops(String source, List<String> drops) {
		data.set(source, drops);
		save();
	}

	public void clearDrops(String source) {
		data.set(source, null);
		save();
	}

	public void addDrop(String source, String dropString) {
		List<String> drops = getDrops(source);
		if (drops == null) {
			drops = new ArrayList<String>();
		}

		drops.add(dropString);

		setDrops(source, drops);
	}
}
