package me.mathiaseklund.survivalrpg.blocks;

import java.util.List;

import org.bukkit.Location;
import org.bukkit.Material;
import org.bukkit.block.Block;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.block.BlockBreakEvent;
import org.bukkit.inventory.ItemStack;

import me.mathiaseklund.survivalrpg.Main;

public class BlockListener implements Listener {

	Main main = Main.getMain();
	BlockManager manager = Main.getBlockManager();

	@SuppressWarnings("unused")
	@EventHandler
	public void onBlockBreak(BlockBreakEvent event) {
		Player player = event.getPlayer();
		Block block = event.getBlock();

		Location loc = block.getLocation();
		/*
		 * if (manager.hasPlaced(player, block)) { Util.debug("Player placed object.");
		 * ItemStack is = new ItemStack(block.getType());
		 * loc.getWorld().dropItemNaturally(loc, is); } else {
		 */
		List<ItemStack> drops = manager.getDrops(event.getBlock(),
				event.getPlayer().getInventory().getItemInMainHand());
		if (drops != null) {
			if (!drops.isEmpty()) {
				for (ItemStack is : drops) {
					if (is != null) {
						if (is.getType() != Material.AIR) {
							loc.getWorld().dropItemNaturally(loc, is);
						}
					}
				}
			}
			// }
		}

		event.setDropItems(false);
		event.setExpToDrop(0);
	}
}
