package me.mathiaseklund.survivalrpg.chat;

import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.player.AsyncPlayerChatEvent;

import me.mathiaseklund.survivalrpg.Main;

public class ChatListener implements Listener {

	ChatManager manager = Main.getChatManager();

	@EventHandler
	public void onChat(AsyncPlayerChatEvent event) {
		String message = event.getMessage();
		Player player = event.getPlayer();
		event.setCancelled(true);
		manager.sendMessage(player, message);
	}
}
