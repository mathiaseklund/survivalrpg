package me.mathiaseklund.survivalrpg.chat;

import org.bukkit.Bukkit;
import org.bukkit.entity.Player;

import me.mathiaseklund.survivalrpg.Main;
import net.md_5.bungee.api.ChatColor;
import net.md_5.bungee.api.chat.ComponentBuilder;
import net.md_5.bungee.api.chat.HoverEvent;
import net.md_5.bungee.api.chat.TextComponent;

public class ChatManager {

	Main main;

	public ChatManager() {
		main = Main.getMain();
	}

	public void sendMessage(Player player, String message) {
		String format = main.getConfig().getString("chat-format");
		format = ChatColor.translateAlternateColorCodes('&', format);
		TextComponent text = new TextComponent();

		TextComponent name = new TextComponent(ChatColor.translateAlternateColorCodes('&',
				main.getConfig().getString("chat-format.name").replaceAll("%name%", player.getDisplayName())));
		name.setHoverEvent(new HoverEvent(HoverEvent.Action.SHOW_TEXT,
				new ComponentBuilder(
						"Level: " + Main.getUserManager().getUser(player.getUniqueId().toString()).getLevel())
								.create()));

		TextComponent divider = new TextComponent(
				ChatColor.translateAlternateColorCodes('&', main.getConfig().getString("chat-format.divider")));

		TextComponent msg = new TextComponent(ChatColor.translateAlternateColorCodes('&',
				main.getConfig().getString("chat-format.message").replaceAll("%message%", message)));

		text.addExtra(name);
		text.addExtra(divider);
		text.addExtra(msg);
		Bukkit.spigot().broadcast(text);
	}
}
