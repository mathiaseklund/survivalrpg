package me.mathiaseklund.survivalrpg.anvil;

import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.inventory.InventoryClickEvent;
import org.bukkit.event.inventory.InventoryType;
import org.bukkit.event.inventory.PrepareAnvilEvent;
import org.bukkit.event.player.PlayerExpChangeEvent;
import org.bukkit.event.player.PlayerLevelChangeEvent;
import org.bukkit.inventory.InventoryView;

import me.mathiaseklund.survivalrpg.Main;

public class AnvilListener implements Listener {

	Main main = Main.getMain();

	@EventHandler
	public void onAnvilClick(InventoryClickEvent event) {
		InventoryView view = event.getView();
		if (view.getTopInventory().getType() == InventoryType.ANVIL) {
			// AnvilInventory inv = (AnvilInventory) view.getTopInventory();
			// Clicked in Anvil inv
		}
	}

	@EventHandler
	public void onPlayerExpChange(PlayerExpChangeEvent event) {
		InventoryView view = event.getPlayer().getOpenInventory();
		if (view != null) {
			event.getPlayer()
					.setLevel(Main.getUserManager().getUser(event.getPlayer().getUniqueId().toString()).getLevel());

		}
	}

	@EventHandler
	public void onPlayerLevelChange(PlayerLevelChangeEvent event) {
		InventoryView view = event.getPlayer().getOpenInventory();
		if (view != null) {

			event.getPlayer()
					.setLevel(Main.getUserManager().getUser(event.getPlayer().getUniqueId().toString()).getLevel());

		}

	}

	@EventHandler
	public void onPrepareAnvil(PrepareAnvilEvent event) {
		// Item added to anvil
	}
}
