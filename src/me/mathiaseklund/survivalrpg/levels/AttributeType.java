package me.mathiaseklund.survivalrpg.levels;

public enum AttributeType {
	STR, STA, DEX
}
