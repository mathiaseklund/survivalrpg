package me.mathiaseklund.survivalrpg.levels;

import java.io.BufferedReader;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.util.HashMap;

import me.mathiaseklund.survivalrpg.Main;
import me.mathiaseklund.survivalrpg.util.Util;

public class ExperienceAPI {

	static HashMap<Integer, Long> required = new HashMap<Integer, Long>();

	public static long getRequiredExp(int level) {
		if (required.isEmpty()) {
			loadRequiredExperience();
		}

		if (required.containsKey(level)) {
			return required.get(level);
		} else {
			return -1;
		}
	}

	public static long getRemainingExperience(int level, long experience) {
		if (required.isEmpty()) {
			loadRequiredExperience();
		}
		long remaining = required.get(level);
		if (remaining >= experience) {
			remaining -= experience;
		} else {
			remaining = 0;
		}
		return remaining;
	}

	public static void loadRequiredExperience() {
		BufferedReader reader;
		try {
			reader = new BufferedReader(new FileReader(Main.getMain().getDataFolder() + "/levels.txt"));
			String line = reader.readLine();
			int i = 0;
			while (line != null) {
				i++;
				required.put(i, Long.parseLong(line));
				line = reader.readLine();
			}
			reader.close();
		} catch (FileNotFoundException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		}
	}

	public static long getExperienceGainedFromKill(int plevel, int mlevel) {
		long experience = 0;

		long base = Main.getMain().getConfig().getLong("base-exp-reward");
		double basemod = Main.getMain().getConfig().getDouble("exp-level-difference");
		double mod = 0;

		if (plevel > mlevel) {
			// Lower EXP Gain
			int diff = plevel - mlevel;
			if (diff > 3) {
				experience = 0;
			} else {
				for (int i = 0; i < diff; i++) {
					mod += basemod;
				}
				double percent = 1.0;
				percent -= mod;
				percent = percent * 100;
				Util.debug("Percent: " + percent);
				long loss = (long) ((long) base - (base * (percent / 100)));
				Util.debug("Loss: " + loss);
				experience = base - loss;
			}
		} else if (plevel == mlevel) {
			experience = base;
		} else if (plevel < mlevel) {
			// Higher EXP Gain
			int diff = mlevel - plevel;
			Util.debug("DIFF: " + diff);

			for (int i = 0; i < diff; i++) {
				mod = mod + basemod;
			}
			Util.debug("Mod: " + mod);
			long gain = (long) (base * mod);
			Util.debug("Gain: " + gain);
			experience = (long) (base + (base * mod));

		}

		Util.debug("Experience Gained: " + experience);

		return experience;
	}
}
